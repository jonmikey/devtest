﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using DevTest.Utils;

namespace DevTest.Models
{
    public class Person
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }

        [EmailAddress]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        public int Age
        {
            get {
                DateTime today = DateTime.Today;
                // The calcAge logic is much easier to test with direct control over what date
                // "today" is.  So, I made it a static utility method for which I can easily control
                // the inputs.  Mocking out DateTime.Today would have been a good way to go
                // as well, but I'm not quite there yet with my C# skills.
                return Utilities.calcAge(this.BirthDate, today);
            }
        }
    }

    public class PersonDBContext : DbContext
    {
        public DbSet<Person> People { get; set; }
    }
}