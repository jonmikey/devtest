﻿using System;

namespace DevTest.Utils
{
    public static class Utilities
    {
        /// <summary>
        /// calcAge calculates the number of birthdays a Person has had.
        /// The calcAge method seemed much easier to test if we built it so that it accepts today's date as
        /// a param, rather than obtaining today's date in the method.
        /// Of course, there may be a way to mock the Today property of DateTime, but my paltry C# skillz
        /// aren't fancy enough yet.
        /// So, you currently have to obtain today's date before calling the method and then pass it in, along with
        /// the birthdate of the person for whom you are calculating age.
        /// </summary>
        /// <param name="birthdate"></param>
        /// <param name="today"></param>
        /// <returns>An integer value representing the number of past birthdays for a person born on the date in
        /// the birthdate param.
        /// </returns>
        public static int calcAge(DateTime birthdate, DateTime today)
        {
            if (today.Date < birthdate.Date)
            {
                return 0;
            }
            int age = today.Year - birthdate.Year;
            if (age > 0 && today.DayOfYear < birthdate.DayOfYear)
            {
                age--;
            }
            return age;
        }
    }
}
