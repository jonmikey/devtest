﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;

using DevTest.Utils;

namespace DevTest.Tests.Controllers
{
    /// <summary>
    /// CalcAgeTest is a suite of tests for Utilities.calcAge method.
    /// </summary>
    [TestClass]
    public class CalcAgeTest
    {
        /// <summary>
        /// Sanity check.  Test with some ordinary inputs.
        /// </summary>
        [TestMethod]
        public void TestOrdinaryInputs()
        {
            DateTime birthdate = new DateTime(1979, 4, 12);
            DateTime today = new DateTime(2014, 7, 6);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(35, age);
        }

        /// <summary>
        /// Test with leap day birthday, and today is March 1st in a non-leap year.
        /// </summary>
        [TestMethod]
        public void TestBornOnLeapDayOn3_1()
        {
            DateTime birthdate = new DateTime(2008, 2, 29);
            DateTime today = new DateTime(2014, 3, 1);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(6, age);
        }

        /// <summary>
        /// Test with leap day bday, and today is March 2nd in a non-leap year.
        /// </summary>
        [TestMethod]
        public void TestBornOnLeapDayOn3_2()
        {
            DateTime birthdate = new DateTime(2008, 2, 29);
            DateTime today = new DateTime(2014, 3, 2);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(6, age);
        }

        /// <summary>
        /// Test with leap day bday, and today is February 28th in a non-leap year.
        /// </summary>
        [TestMethod]
        public void TestBornOnLeapDayOn2_28()
        {
            DateTime birthdate = new DateTime(2008, 2, 29);
            DateTime today = new DateTime(2014, 2, 28);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(5, age);
        }

        /// <summary>
        /// Test with a birthdate that occurs after today.
        /// </summary>
        [TestMethod]
        public void TestNotBornYet()
        {
            DateTime birthdate = new DateTime(2020, 1, 1);
            DateTime today = new DateTime(2014, 7, 6);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(0, age);
        }

        /// <summary>
        /// Test with a birthdate for someone who is less than 1 year old.
        /// </summary>
        [TestMethod]
        public void TestLessThan1YearOld()
        {
            DateTime birthdate = new DateTime(2014, 1, 1);
            DateTime today = new DateTime(2014, 7, 6);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(0, age);
        }

        /// <summary>
        /// Test with a birthdate exactly one year before the value of today.
        /// </summary>
        [TestMethod]
        public void TestExactlyOneYearOld()
        {
            DateTime birthdate = new DateTime(2013, 5, 5);
            DateTime today = new DateTime(2014, 5, 5);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(1, age);
        }

        /// <summary>
        /// Test with a birthdate that is exactly one year and one day before today.
        /// </summary>
        [TestMethod]
        public void Test1DayOver1YearOld()
        {
            DateTime birthdate = new DateTime(2013, 5, 5);
            DateTime today = new DateTime(2014, 5, 6);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(1, age);
        }

        /// <summary>
        /// Test with a birthdate and time that is exactly one hour less than one year ago. 
        /// </summary>
        [TestMethod]
        public void Test1HourUnder1YearOld()
        {
            DateTime birthdate = new DateTime(2013, 5, 5, 8, 0, 0);
            DateTime today = new DateTime(2014, 5, 5, 7, 0, 0);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(1, age);
        }

        /// <summary>
        /// Test with a birthdate and time that is exactly one hour more than one year ago.
        /// </summary>
        [TestMethod]
        public void Test1HourOver1YearOld()
        {
            DateTime birthdate = new DateTime(2013, 5, 5, 8, 0, 0);
            DateTime today = new DateTime(2014, 5, 5, 9, 0, 0);
            int age = Utilities.calcAge(birthdate, today);
            Assert.AreEqual(1, age);
        }
    }
}
